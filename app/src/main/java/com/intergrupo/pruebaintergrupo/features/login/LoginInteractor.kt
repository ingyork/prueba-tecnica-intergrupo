package com.intergrupo.pruebaintergrupo.features.login


import com.google.gson.Gson
import com.intergrupo.pruebaintergrupo.data.db.DBHelper
import com.intergrupo.pruebaintergrupo.data.db.entities.AuthEntity
import com.intergrupo.pruebaintergrupo.data.network.IntergrupoApiFactory
import com.intergrupo.pruebaintergrupo.data.network.dtos.AuthDTO
import com.intergrupo.pruebaintergrupo.utils.preference.PreferencesMethods
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface ILoginInteractor {
    fun login(username: String, password: String, isRemember: Boolean)
    fun isRememberSession()
}

class LoginInteractor: ILoginInteractor {

    interface OnLoginFinishedListener {
        fun loginFail()
        fun onSuccess()
        fun onRememberSession()
    }

    var listener: OnLoginFinishedListener? = null

    constructor(listener: OnLoginFinishedListener){
        this@LoginInteractor.listener = listener
    }

    override fun login(username: String, password: String, isRemember: Boolean) {

        IntergrupoApiFactory
                .createAuth()
                ?.authentication(username, password)
                ?.subscribeOn(Schedulers.newThread())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    it?.let {
                        if(it.body().toString().contains("success")){
                            val gs = Gson()
                            val auth = gs.fromJson(it.body().toString(), AuthDTO::class.java)
                            PreferencesMethods.updateTokenAuth(auth.token)
                            val ent = AuthEntity(1, auth.token, auth.email, isRemember)
                            DBHelper().insertRow(ent)
                            listener?.onSuccess()
                        }
                        else{
                            listener?.loginFail()
                        }
                    }
                }
    }

    override fun isRememberSession() {
        val realmDb = DBHelper()
        if(realmDb.isNotEmptyTable(AuthEntity::class.java)) {
            realmDb.getRows(AuthEntity::class.java)?.first()?.let {
                if (it.isRemember)
                    listener?.onRememberSession()
            }
        }
    }
}