package com.intergrupo.pruebaintergrupo.features.prospect.edit

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.intergrupo.pruebaintergrupo.R
import com.intergrupo.pruebaintergrupo.data.db.entities.ProspectEntity
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import com.intergrupo.pruebaintergrupo.di.prospect.DaggerProspectComponent
import com.intergrupo.pruebaintergrupo.features.login.LoginActivity
import kotlinx.android.synthetic.main.activity_edit_prospect.*
import javax.inject.Inject

class EditProspectActivity : AppCompatActivity(), EditProspectView {

    @Inject
    lateinit var presenter: IEditProspectPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_prospect)


        var propId = ""
        val extras = intent.extras
        if (extras != null) {
            propId = extras.getString("id")
        }

        DaggerProspectComponent.builder().build().inject(this)

        presenter.attach(this)
        presenter.getProspectById(propId)

        btnUpdate.setOnClickListener { onActionUpdate() }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                presenter.logout()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    override fun returnProspect(prop: ProspectDTO) {
        txtId.text = prop.id
        txtName.setText(prop.name)
        txtSurname.setText(prop.surname)
        txtTelephone.setText(prop.telephone)
        txtAddress.setText(prop.address)
        txtStatusCode.setText(prop.sectionCode)
        txtZoneCode.setText(prop.zoneCode)
        txtNeighborhoodCode.setText(prop.neighborhoodCode)
        txtCityCode.setText(prop.cityCode)
        txtSectionCode.setText(prop.sectionCode)
        txtRoleId.setText(prop.roleId.toString())
        txtAppoinTableId.setText(prop.appointableId.toString())
        txtRejectedObservation.setText(prop.rejectedObservation.toString())
        txtObservation.setText(prop.observation)
    }

    override fun saveSuccess() {
        val i = intent
        setResult(RESULT_OK, i)
        finish()
    }

    override fun logout() {
        val intent = Intent(this, LoginActivity::class.java)
        finishAffinity()
        startActivity(intent)
    }

    fun onActionUpdate() {
        if (submitForm()) {
            val p = ProspectEntity()
            p.id = txtId.text.toString()
            p.name = txtName.text.toString()
            p.surname = txtSurname.text.toString()
            p.telephone = txtTelephone.text.toString()
            p.address = txtAddress.text.toString()
            p.zoneCode = txtZoneCode.text.toString()
            p.neighborhoodCode = txtNeighborhoodCode.text.toString()
            p.cityCode = txtCityCode.text.toString()
            p.sectionCode = txtSectionCode.text.toString()
            p.roleId = txtRoleId.text.toString().toInt()
            p.appointableId = txtAppoinTableId.text.toString().toInt()
            p.rejectedObservation = txtRejectedObservation.text.toString().toInt()
            p.observation = txtObservation.text.toString()
            presenter.updateProspect(p)
        }
    }

    private fun submitForm(): Boolean {

        if (!validateName()) {
            return false
        }
        if (!validateSurName()) {
            return false
        }
        if (!validateTelephone()) {
            return false
        }
        if (!validateAddress()) {
            return false
        }
        if (!validateStatusCode()) {
            return false
        }
        if (!validateZoneCode()) {
            return false
        }
        if (!validateNeighborhoodCode()) {
            return false
        }
        if (!validateCityCode()) {
            return false
        }
        if (!validateSectionCode()) {
            return false
        }
        if (!validateRoleId()) {
            return false
        }
        if (!validateAppoinTableId()) {
            return false
        }
        if (!validateRejectedObservation()) {
            return false
        }
        return if (!validateObservation()) {
            false
        } else true

    }

    private inner class EditTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.txtName -> validateName()
                R.id.txtSurname -> validateSurName()
                R.id.txtTelephone -> validateTelephone()
                R.id.txtAddress -> validateAddress()
                R.id.txtStatusCode -> validateStatusCode()
                R.id.txtZoneCode -> validateZoneCode()
                R.id.txtNeighborhoodCode -> validateNeighborhoodCode()
                R.id.txtCityCode -> validateCityCode()
                R.id.txtSectionCode -> validateSectionCode()
                R.id.txtRoleId -> validateRoleId()
                R.id.txtAppoinTableId -> validateAppoinTableId()
                R.id.txtRejectedObservation -> validateRejectedObservation()
                R.id.txtObservation -> validateObservation()
            }
        }
    }
}
