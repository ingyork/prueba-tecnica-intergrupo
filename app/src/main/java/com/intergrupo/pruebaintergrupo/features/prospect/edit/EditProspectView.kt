package com.intergrupo.pruebaintergrupo.features.prospect.edit

import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO

interface EditProspectView {
    fun returnProspect(prop: ProspectDTO)
    fun saveSuccess()
    fun logout()
}