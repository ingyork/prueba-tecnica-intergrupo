package com.intergrupo.pruebaintergrupo.features.main

import com.intergrupo.pruebaintergrupo.base.mvp.BasePresenter
import com.intergrupo.pruebaintergrupo.base.mvp.IBasePresenter
import com.intergrupo.pruebaintergrupo.di.main.DaggerMainComponent
import com.intergrupo.pruebaintergrupo.di.main.MainModule
import javax.inject.Inject

interface IMainPresenter: IBasePresenter<MainView> {
    fun logout()
    fun onLogout()
}

class MainPresenter: BasePresenter<MainView>, IMainPresenter, MainInteractor.OnMainListener {

    @Inject
    lateinit var mainModel: IMainInteractor

    constructor() {
        DaggerMainComponent.builder().mainModule(MainModule(this)).build().inject(this)
    }

    override fun logout() {
        mainModel?.logout()
    }

    override fun onLogout() {
        view?.logout()
    }
}