package com.intergrupo.pruebaintergrupo.features.prospect.edit

import com.intergrupo.pruebaintergrupo.data.db.DBHelper
import com.intergrupo.pruebaintergrupo.data.db.entities.AuthEntity
import com.intergrupo.pruebaintergrupo.data.db.entities.ProspectEntity
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO

interface IEditProspectInteractor {
    fun getProspectById(id: String)
    fun updateProspect(prospectForUpdate: ProspectEntity)
    fun logout()
}

class EditProspectInteractor: IEditProspectInteractor {


    interface OnEditProspectListener {
        fun OnUpdateProspect()
        fun OnGetProspectById(prop: ProspectDTO?)
        fun OnLogout()
    }

    var listener: OnEditProspectListener? = null

    constructor(listener: OnEditProspectListener) {
        this.listener = listener
    }

    override fun getProspectById(id: String) {
        val realmDb = DBHelper()
        var prop = realmDb.getRowBy(id, ProspectEntity::class.java)
        prop?.let {
            it.first()?.let {
                val ent = it
                var dto = ProspectDTO()
                dto.id = ent.id
                dto.name = ent.name
                dto.surname = ent.surname
                dto.telephone = ent.telephone
                dto.schProspectIdentification = ent.schProspectIdentification
                dto.address = ent.address
                dto.statusCd = if (ent.statusCd == null) 0 else ent.statusCd!!
                dto.zoneCode = ent.zoneCode
                dto.neighborhoodCode = ent.neighborhoodCode
                dto.cityCode = ent.cityCode
                dto.sectionCode = ent.sectionCode
                dto.roleId = if (ent.roleId == null) 0 else ent.roleId!!
                dto.appointableId = if (ent.appointableId == null) 0 else ent.appointableId!!
                dto.rejectedObservation = if (ent.rejectedObservation == null) 0 else ent.rejectedObservation!!
                dto.observation = ent.observation
                dto.isDisable = if(ent.isDisable != null) ent.isDisable!! else false
                dto.isVisited = if(ent.isVisited != null) ent.isVisited!! else false
                dto.isCallcenter = if(ent.isCallcenter != null) ent.isCallcenter!! else false
                dto.isAcceptSearch = if(ent.isAcceptSearch != null) ent.isAcceptSearch!! else false
                dto.campaignCode = ent.campaignCode
                dto.userId = if (ent.userId == null) 0 else ent.userId!!

                listener?.OnGetProspectById(dto)
            }
        }
        realmDb.close()
    }

    override fun updateProspect(prospectForUpdate: ProspectEntity) {
        prospectForUpdate.id?.let {
            val realmDb = DBHelper()
            val prospDb = realmDb.getRowBy(it, ProspectEntity::class.java)
            prospDb?.first()?.let {
                prospectForUpdate.schProspectIdentification = it.schProspectIdentification
                prospectForUpdate.statusCd = it.statusCd
                prospectForUpdate.isDisable = it.isDisable
                prospectForUpdate.isVisited = it.isVisited
                prospectForUpdate.isCallcenter = it.isCallcenter
                prospectForUpdate.isAcceptSearch = it.isAcceptSearch
                prospectForUpdate.campaignCode = it.campaignCode
                prospectForUpdate.userId = it.userId
                realmDb.insertRow(prospectForUpdate)
            }
            realmDb.close()
        }
        listener?.OnUpdateProspect()
    }

    override fun logout() {
        val realmDb = DBHelper()
        if(realmDb.isNotEmptyTable(AuthEntity::class.java)) {
            realmDb.getRows(AuthEntity::class.java)?.first()?.let {
                realmDb.deleteAll(AuthEntity::class.java)
                listener?.OnLogout()
            }
        }
    }
}