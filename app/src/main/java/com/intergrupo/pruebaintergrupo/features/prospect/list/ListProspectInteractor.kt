package com.intergrupo.pruebaintergrupo.features.prospect.list

import com.intergrupo.pruebaintergrupo.data.db.DBHelper
import com.intergrupo.pruebaintergrupo.data.db.entities.ProspectEntity
import com.intergrupo.pruebaintergrupo.data.network.IntergrupoApiFactory
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import com.intergrupo.pruebaintergrupo.utils.map.MapUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface IListProspectInteractor {
    fun listProspects()
}

class ListProspectInteractor: IListProspectInteractor {

    interface OnProspectListener {
        fun onEmptyData()
        fun onSuccessListProspects(props: List<ProspectDTO>)
    }

    var listener: OnProspectListener? = null

    constructor(listener: OnProspectListener) {
        this.listener = listener
    }

    override fun listProspects() {

        val realmDb = DBHelper()
        if(realmDb.isEmptyTable(ProspectEntity::class.java)){

            IntergrupoApiFactory.createProspect()
                    .getProspects()
                    ?.subscribeOn(Schedulers.newThread())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe {
                        it?.let {
                            val entities = MapUtil.getEntityFromListDto(it)
                            entities?.let {
                                realmDb.insertRows(entities)
                            }
                            listener?.onSuccessListProspects(it)
                        }
                    }
        }
        else {
            realmDb.getRows(ProspectEntity::class.java)?.let {
                val dtos = MapUtil.getDtosFromEntities(it)
                dtos?.let {
                    listener?.onSuccessListProspects(it)
                }
            }
            realmDb.close()
        }
    }
}