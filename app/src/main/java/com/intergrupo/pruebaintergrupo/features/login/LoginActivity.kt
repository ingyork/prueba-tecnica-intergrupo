package com.intergrupo.pruebaintergrupo.features.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.intergrupo.pruebaintergrupo.R
import com.intergrupo.pruebaintergrupo.di.login.DaggerLoginComponent
import com.intergrupo.pruebaintergrupo.di.login.LoginModule
import com.intergrupo.pruebaintergrupo.features.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginView {

    @Inject
    lateinit var presenter: ILoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        btnLogin.setOnClickListener { loginAction() }


        DaggerLoginComponent.builder()
                .loginModule(LoginModule())
                .build().inject(this)

        presenter.attach(this)
        presenter.isRememberSession()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    private fun loginAction(){
        val username = txtUsername.text.toString().trim({ it <= ' ' })
        val password = txtPassword.text.toString().trim({ it <= ' ' })

        var isEmpty = true

        presenter?.validateCredentials(username, password).let {
            if(it!!){
                isEmpty = false
            }
        }


        presenter?.validateEmail(username).let {
            if(it != null){
                if(!isEmpty && it)
                    presenter?.login(username, password, true)
                else
                    Toast.makeText(this, "Formato de email Erroneo", Toast.LENGTH_LONG).show()
            }
        }

    }

    override fun messageCredentialsError() {
        Toast.makeText(this, "Credenciales Incorrectas", Toast.LENGTH_LONG).show()
    }

    override fun success() {
        goToMain()
    }

    override fun rememberSession() {
        goToMain()
    }

    fun goToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
