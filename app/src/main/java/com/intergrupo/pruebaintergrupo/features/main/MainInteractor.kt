package com.intergrupo.pruebaintergrupo.features.main

import com.intergrupo.pruebaintergrupo.data.db.DBHelper
import com.intergrupo.pruebaintergrupo.data.db.entities.AuthEntity


interface IMainInteractor {
    fun logout()
}

class MainInteractor: IMainInteractor {

    interface OnMainListener {
        fun onLogout()
    }

    var listener: OnMainListener? = null

    constructor(listener: OnMainListener) {
        this.listener = listener
    }

    override fun logout() {
        val realmDb = DBHelper()
        if(realmDb.isNotEmptyTable(AuthEntity::class.java)) {
            realmDb.getRows(AuthEntity::class.java)?.first()?.let {
                realmDb.deleteAll(AuthEntity::class.java)
                listener?.onLogout()
            }
        }
    }
}