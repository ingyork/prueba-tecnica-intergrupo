package com.intergrupo.pruebaintergrupo.features.login

interface LoginView {
    fun messageCredentialsError()
    fun success()
    fun rememberSession()
}