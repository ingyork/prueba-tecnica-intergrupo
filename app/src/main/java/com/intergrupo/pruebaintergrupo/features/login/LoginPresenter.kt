package com.intergrupo.pruebaintergrupo.features.login

import com.intergrupo.pruebaintergrupo.base.mvp.BasePresenter
import com.intergrupo.pruebaintergrupo.base.mvp.IBasePresenter
import com.intergrupo.pruebaintergrupo.di.login.DaggerLoginComponent
import com.intergrupo.pruebaintergrupo.di.login.LoginModule
import java.util.regex.Pattern
import javax.inject.Inject

interface ILoginPresenter: IBasePresenter<LoginView> {
    fun validateCredentials(username: String, password: String): Boolean
    fun validateEmail(username: String): Boolean
    fun login(username: String, password: String, remember: Boolean)
    fun isRememberSession()
    fun onRememberSession()
    fun onSuccess()
    fun loginFail()
}

class LoginPresenter: BasePresenter<LoginView>, ILoginPresenter, LoginInteractor.OnLoginFinishedListener {

    @Inject
    lateinit var loginModel: ILoginInteractor

    constructor() {
        DaggerLoginComponent.builder().loginModule(LoginModule(this)).build().inject(this)
    }

    override fun validateCredentials(username: String, password: String): Boolean {
        var response = false
        if (username.isNotEmpty() && password.isNotEmpty()) {
            response = true
        } else {
            view?.messageCredentialsError()
        }
        return response
    }

    override fun validateEmail(username: String): Boolean {
        val email_regx = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$"
        val pattern = Pattern.compile(email_regx)
        val matcher = pattern.matcher(username)
        return matcher.matches()
    }

    override fun login(username: String, password: String, remember: Boolean) {
        loginModel?.login(username, password, remember)
    }

    override fun isRememberSession() {
        loginModel?.isRememberSession()
    }

    /*Methods of Interactor*/
    override fun loginFail() {
        view?.messageCredentialsError()
    }

    override fun onSuccess() {
        view?.success()
    }

    override fun onRememberSession() {
        view?.rememberSession()
    }
}