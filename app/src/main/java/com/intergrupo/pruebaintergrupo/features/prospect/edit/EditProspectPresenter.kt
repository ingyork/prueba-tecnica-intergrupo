package com.intergrupo.pruebaintergrupo.features.prospect.edit

import com.intergrupo.pruebaintergrupo.base.mvp.BasePresenter
import com.intergrupo.pruebaintergrupo.base.mvp.IBasePresenter
import com.intergrupo.pruebaintergrupo.data.db.entities.ProspectEntity
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import com.intergrupo.pruebaintergrupo.di.prospect.DaggerProspectComponent
import com.intergrupo.pruebaintergrupo.di.prospect.ProspectModule
import javax.inject.Inject

interface IEditProspectPresenter: IBasePresenter<EditProspectView> {
    fun getProspectById(id: String)
    fun OnGetProspectById(prop: ProspectDTO?)
    fun updateProspect(prospect: ProspectEntity)
    fun OnUpdateProspect()
    fun logout()
}

class EditProspectPresenter: BasePresenter<EditProspectView>, EditProspectInteractor.OnEditProspectListener, IEditProspectPresenter {

    @Inject
    lateinit var prospectModel: IEditProspectInteractor

    constructor() {
        DaggerProspectComponent.builder().prospectModule(ProspectModule(this)).build().inject(this)
    }

    override fun OnUpdateProspect() {
        view?.saveSuccess()
    }

    override fun OnGetProspectById(prop: ProspectDTO?) {
        prop?.let {
            view?.returnProspect(it)
        }
    }

    override fun OnLogout() {
        view?.logout()
    }

    override fun getProspectById(id: String) {
        prospectModel?.getProspectById(id)
    }

    override fun updateProspect(prospect: ProspectEntity){
        prospectModel?.updateProspect(prospect)
    }

    override fun logout(){
        prospectModel?.logout()
    }
}