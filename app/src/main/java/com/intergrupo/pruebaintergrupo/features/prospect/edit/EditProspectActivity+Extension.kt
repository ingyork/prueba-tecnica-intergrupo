package com.intergrupo.pruebaintergrupo.features.prospect.edit

import com.intergrupo.pruebaintergrupo.R
import kotlinx.android.synthetic.main.activity_edit_prospect.*


fun EditProspectActivity.validateName(): Boolean {
    if (txtName.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilName.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilName.setErrorEnabled(false)
    }
    return true
}


fun EditProspectActivity.validateSurName(): Boolean {
    if (txtSurname.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilSurname.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilSurname.setErrorEnabled(false)
    }
    return true
}


fun EditProspectActivity.validateTelephone(): Boolean {
    if (txtTelephone.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilTelephone.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilTelephone.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateAddress(): Boolean {
    if (txtAddress.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilAddress.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilAddress.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateStatusCode(): Boolean {
    if (txtStatusCode.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilStatusCode.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilStatusCode.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateZoneCode(): Boolean {
    if (txtZoneCode.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilZoneCode.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilZoneCode.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateNeighborhoodCode(): Boolean {
    if (txtNeighborhoodCode.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilNeighborhoodCode.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilNeighborhoodCode.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateCityCode(): Boolean {
    if (txtCityCode.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilCityCode.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilCityCode.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateSectionCode(): Boolean {
    if (txtSectionCode.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilSectionCode.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilSectionCode.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateRoleId(): Boolean {
    if (txtRoleId.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilRoleId.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilRoleId.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateAppoinTableId(): Boolean {
    if (txtAppoinTableId.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilAppoinTableId.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilAppoinTableId.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateRejectedObservation(): Boolean {
    if (txtRejectedObservation.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilRejectedObservation.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilRejectedObservation.setErrorEnabled(false)
    }
    return true
}

fun EditProspectActivity.validateObservation(): Boolean {
    if (txtObservation.getText().toString().trim({ it <= ' ' }).isEmpty()) {
        ilObservation.setError(getString(R.string.err_msg_required))
        return false
    } else {
        ilObservation.setErrorEnabled(false)
    }
    return true
}