package com.intergrupo.pruebaintergrupo.features.prospect.list

import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO

interface ListProspectView {
    fun listProspect(prospects: List<ProspectDTO>)
}