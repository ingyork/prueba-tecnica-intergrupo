package com.intergrupo.pruebaintergrupo.features.prospect.list

import com.intergrupo.pruebaintergrupo.base.mvp.BasePresenter
import com.intergrupo.pruebaintergrupo.base.mvp.IBasePresenter
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import com.intergrupo.pruebaintergrupo.di.prospect.DaggerProspectComponent
import com.intergrupo.pruebaintergrupo.di.prospect.ProspectModule
import javax.inject.Inject


interface IListProspectPresenter: IBasePresenter<ListProspectView> {
    fun listProspects()
    fun onSuccessListProspects(props: List<ProspectDTO>)
    fun onEmptyData()
}

class ListProspectPresenter: BasePresenter<ListProspectView>, IListProspectPresenter, ListProspectInteractor.OnProspectListener {

    @Inject
    lateinit var prospectModel: IListProspectInteractor

    constructor() {
        DaggerProspectComponent.builder().prospectModule(ProspectModule(this)).build().inject(this)
    }

    override fun onSuccessListProspects(props: List<ProspectDTO>) {
        view?.listProspect(props)
    }

    override fun onEmptyData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun listProspects() {
        prospectModel?.listProspects()
    }
}