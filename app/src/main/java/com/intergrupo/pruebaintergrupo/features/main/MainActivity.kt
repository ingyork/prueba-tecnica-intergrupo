package com.intergrupo.pruebaintergrupo.features.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import com.intergrupo.pruebaintergrupo.R
import com.intergrupo.pruebaintergrupo.di.main.DaggerMainComponent
import com.intergrupo.pruebaintergrupo.features.login.LoginActivity
import com.intergrupo.pruebaintergrupo.features.prospect.list.ListProspectFragment
import javax.inject.Inject

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, MainView {

    @Inject
    lateinit var presenter: IMainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        nav_view.setNavigationItemSelectedListener(this)

        supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, ListProspectFragment.newInstance(null))
                .commit()

        DaggerMainComponent.builder().build().inject(this)

        presenter.attach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                presenter.logout()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        var fragmentTransaction = false
        var fragment: Fragment? = null
        val id = item.itemId

        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_list_propspect -> {
                fragment = ListProspectFragment.newInstance(null)
                fragmentTransaction = true
            }
            R.id.nav_logout -> {
                presenter.logout()
            }
        }


        if (fragmentTransaction) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun logout() {
        val intent = Intent(this, LoginActivity::class.java)
        finishAffinity()
        startActivity(intent)
    }
}
