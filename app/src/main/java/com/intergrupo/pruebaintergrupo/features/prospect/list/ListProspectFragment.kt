package com.intergrupo.pruebaintergrupo.features.prospect.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.intergrupo.pruebaintergrupo.R
import com.intergrupo.pruebaintergrupo.adapters.ProspectAdapter
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import com.intergrupo.pruebaintergrupo.di.prospect.DaggerProspectComponent
import com.intergrupo.pruebaintergrupo.features.prospect.edit.EditProspectActivity
import kotlinx.android.synthetic.main.fragment_list_prospect.view.*
import javax.inject.Inject

class ListProspectFragment : Fragment(), ListProspectView, ProspectAdapter.OnInteractionItemProspect {

    private lateinit var rootView: View
    private var listener: OnFragmentInteractionListener? = null

    @Inject
    lateinit var presenter: IListProspectPresenter
    internal lateinit var adapter: ProspectAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_list_prospect, container, false)

        rootView.rv_prospects.apply {
            layoutManager =  LinearLayoutManager(activity)
        }


        DaggerProspectComponent.builder().build().inject(this)

        presenter.attach(this)


        messageStatus("loading")
        presenter.listProspects()

        return rootView

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        val UPDATE_REQUEST = 1 // The request code

        @JvmStatic
        fun newInstance(bundle: Bundle?) =
                ListProspectFragment().apply {
                    arguments = bundle
                }
    }


    private fun messageStatus(operation: String) {
        if (operation.equals("loading", ignoreCase = true)) {

            rootView.ivEmpty.setVisibility(View.GONE)
            rootView.rv_prospects.setVisibility(View.GONE)
            rootView.tvMessageStatus.setText("Cargando...")
            rootView.tvMessageStatus.setVisibility(View.VISIBLE)
            rootView.progressStatus.setVisibility(View.VISIBLE)
        } else if (operation.equals("empty", ignoreCase = true)) {

            rootView.rv_prospects.setVisibility(View.GONE)
            rootView.progressStatus.setVisibility(View.GONE)
            rootView.tvMessageStatus.setVisibility(View.GONE)
            rootView.tvMessageStatus.setVisibility(View.GONE)
            rootView.ivEmpty.setVisibility(View.VISIBLE)
        } else if (operation.equals("success", ignoreCase = true)) {
            rootView.ivEmpty.setVisibility(View.GONE)
            rootView.progressStatus.setVisibility(View.GONE)
            rootView.tvMessageStatus.setVisibility(View.GONE)
            rootView.rv_prospects.setVisibility(View.VISIBLE)
        }

    }

    override fun listProspect(prospects: List<ProspectDTO>) {
        adapter = ProspectAdapter(prospects)
        adapter.setOnInteractionItemProspect(this)
        rootView.rv_prospects.setAdapter(adapter)
        messageStatus("success")
    }

    override fun OnEditProspect(prospectId: String) {
        val intent = Intent(activity, EditProspectActivity::class.java)
        intent.putExtra("id", prospectId)
        startActivityForResult(intent, UPDATE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UPDATE_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    messageStatus("loading")
                    presenter.listProspects()
                    Toast.makeText(activity, "Se ha actualizado el registro", Toast.LENGTH_SHORT).show()
                }
            }
        }


    }
}
