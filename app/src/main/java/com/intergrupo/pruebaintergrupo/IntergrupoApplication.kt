package com.intergrupo.pruebaintergrupo

import android.app.Application
import android.content.ContextWrapper
import com.intergrupo.pruebaintergrupo.di.login.DaggerLoginComponent
import com.intergrupo.pruebaintergrupo.utils.preference.PreferencesMethods
import com.intergrupo.pruebaintergrupo.utils.preference.Prefs
import io.realm.Realm

class IntergrupoApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)

        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(PreferencesMethods.KeyPreferences.PREFERENCE_NAME)
                .setUseDefaultSharedPreference(false)
                .build()

    }
}