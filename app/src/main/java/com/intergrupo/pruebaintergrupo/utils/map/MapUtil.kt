package com.intergrupo.pruebaintergrupo.utils.map

import com.intergrupo.pruebaintergrupo.data.db.entities.ProspectEntity
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO

class MapUtil {
    companion object {

        fun getEntityFromDto(dto: ProspectDTO): ProspectEntity? {
            val ent = ProspectEntity(
                    id = dto.id,
                    name = dto.name,
                    surname = dto.surname,
                    telephone = dto.telephone,
                    schProspectIdentification = dto.schProspectIdentification,
                    address = dto.address,
                    statusCd = dto.statusCd,
                    zoneCode = dto.zoneCode,
                    neighborhoodCode = dto.neighborhoodCode,
                    cityCode = dto.cityCode,
                    sectionCode = dto.sectionCode,
                    roleId = dto.roleId,
                    appointableId = dto.appointableId,
                    rejectedObservation = dto.rejectedObservation,
                    observation = dto.observation,
                    isDisable = dto.isDisable,
                    isVisited = dto.isVisited,
                    isCallcenter = dto.isCallcenter,
                    isAcceptSearch = dto.isAcceptSearch,
                    campaignCode = dto.campaignCode,
                    userId = dto.userId
            )
            return ent
        }

        fun getEntityFromListDto(dtos: List<ProspectDTO>): List<ProspectEntity>? {
            val entities = mutableListOf<ProspectEntity>()
            dtos.forEach {
                val ent = ProspectEntity(
                        id = it.id,
                        name = it.name,
                        surname = it.surname,
                        telephone = it.telephone,
                        schProspectIdentification = it.schProspectIdentification,
                        address = it.address,
                        statusCd = it.statusCd,
                        zoneCode = it.zoneCode,
                        neighborhoodCode = it.neighborhoodCode,
                        cityCode = it.cityCode,
                        sectionCode = it.sectionCode,
                        roleId = it.roleId,
                        appointableId = it.appointableId,
                        rejectedObservation = it.rejectedObservation,
                        observation = it.observation,
                        isDisable = it.isDisable,
                        isVisited = it.isVisited,
                        isCallcenter = it.isCallcenter,
                        isAcceptSearch = it.isAcceptSearch,
                        campaignCode = it.campaignCode,
                        userId = it.userId
                )
                entities.add(ent)
            }
            return entities
        }

        fun getDtosFromEntities(ent: List<ProspectEntity>): List<ProspectDTO>? {
            val dtos = mutableListOf<ProspectDTO>()
            ent.forEach {
                val dto = ProspectDTO()
                dto.id = it.id
                dto.name = it.name
                dto.surname = it.surname
                dto.telephone = it.telephone
                dto.schProspectIdentification = it.schProspectIdentification
                dto.address = it.address
                dto.statusCd = if(it.statusCd != null) it.statusCd!! else 0
                dto.zoneCode = it.zoneCode
                dto.neighborhoodCode = it.neighborhoodCode
                dto.cityCode = it.cityCode
                dto.sectionCode = it.sectionCode
                dto.roleId = if(it.roleId != null) it.roleId!! else 0
                dto.appointableId = if(it.appointableId != null) it.appointableId!! else 0
                dto.rejectedObservation = if(it.rejectedObservation != null) it.rejectedObservation!! else 0
                dto.observation = it.observation
                dto.isDisable = if(it.isDisable != null) it.isDisable!! else false
                dto.isVisited = if(it.isVisited != null) it.isVisited!! else false
                dto.isCallcenter = if(it.isCallcenter != null) it.isCallcenter!! else false
                dto.isAcceptSearch = if(it.isAcceptSearch != null) it.isAcceptSearch!! else false
                dto.campaignCode = it.campaignCode
                dto.userId = if(it.userId != null) it.userId!! else 0
                dtos.add(dto)
            }
            return dtos
        }
    }
}