package com.intergrupo.pruebaintergrupo.utils.preference;

/**
 * Created by jorge.castro on 11/18/2017.
 */

public class PreferencesMethods {
    public class KeyPreferences {
        public static final String PREFERENCE_NAME = "applicationPreferences";
        public static final String ACCESS_TOKEN = "access_token";
    }


    public static void updateTokenAuth(String token){
        Prefs.putString(KeyPreferences.ACCESS_TOKEN, token);
    }


    @Override
    public PreferencesMethods clone(){
        try {
            throw new CloneNotSupportedException();
        }catch(CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
