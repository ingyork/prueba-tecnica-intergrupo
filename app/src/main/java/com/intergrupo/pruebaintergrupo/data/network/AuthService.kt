package com.intergrupo.pruebaintergrupo.data.network

import com.intergrupo.pruebaintergrupo.data.network.dtos.AuthDTO
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface AuthService {

    @GET("application/login")
    fun authentication(@Query("email") username: String, @Query("password") password: String): Observable<Response<Any>>
}