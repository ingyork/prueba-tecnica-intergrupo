package com.intergrupo.pruebaintergrupo.data.network.dtos

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ProspectDTO() : Parcelable {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("surname")
    var surname: String? = null
    @SerializedName("telephone")
    var telephone: String? = null
    @SerializedName("schProspectIdentification")
    var schProspectIdentification: String? = null
    @SerializedName("address")
    var address: String? = null
    @SerializedName("createdAt")
    var createdAt: String? = null
    @SerializedName("updatedAt")
    var updatedAt: String? = null
    @SerializedName("statusCd")
    var statusCd: Int = 0
    @SerializedName("zoneCode")
    var zoneCode: String? = null
    @SerializedName("neighborhoodCode")
    var neighborhoodCode: String? = null
    @SerializedName("cityCode")
    var cityCode: String? = null
    @SerializedName("sectionCode")
    var sectionCode: String? = null
    @SerializedName("roleId")
    var roleId: Int = 0
    @SerializedName("appointableId")
    var appointableId: Int = 0
    @SerializedName("rejectedObservation")
    var rejectedObservation: Int = 0
    @SerializedName("observation")
    var observation: String? = null
    @SerializedName("disable")
    var isDisable: Boolean = false
    @SerializedName("visited")
    var isVisited: Boolean = false
    @SerializedName("callcenter")
    var isCallcenter: Boolean = false
    @SerializedName("acceptSearch")
    var isAcceptSearch: Boolean = false
    @SerializedName("campaignCode")
    var campaignCode: String? = null
    @SerializedName("userId")
    var userId: Int = 0

    constructor(parcel: Parcel): this(){
        readFromParcel(parcel)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(surname)
        parcel.writeString(telephone)
        parcel.writeString(schProspectIdentification)
        parcel.writeString(address)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeInt(statusCd)
        parcel.writeString(zoneCode)
        parcel.writeString(neighborhoodCode)
        parcel.writeString(cityCode)
        parcel.writeString(sectionCode)
        parcel.writeInt(roleId)
        parcel.writeInt(appointableId)
        parcel.writeInt(rejectedObservation)
        parcel.writeString(observation)
        parcel.writeByte(if (isDisable) 1 else 0)
        parcel.writeByte(if (isVisited) 1 else 0)
        parcel.writeByte(if (isCallcenter) 1 else 0)
        parcel.writeByte(if (isAcceptSearch) 1 else 0)
        parcel.writeString(campaignCode)
        parcel.writeInt(userId)
    }

    private fun readFromParcel(parcel: Parcel) {
        id = parcel.readString()
        name = parcel.readString()
        surname = parcel.readString()
        telephone = parcel.readString()
        schProspectIdentification = parcel.readString()
        address = parcel.readString()
        createdAt = parcel.readString()
        updatedAt = parcel.readString()
        statusCd = parcel.readInt()
        zoneCode = parcel.readString()
        neighborhoodCode = parcel.readString()
        cityCode = parcel.readString()
        sectionCode = parcel.readString()
        roleId = parcel.readInt()
        appointableId = parcel.readInt()
        rejectedObservation = parcel.readInt()
        observation = parcel.readString()
        isDisable = parcel.readByte() != 0.toByte()
        isVisited = parcel.readByte() != 0.toByte()
        isCallcenter = parcel.readByte() != 0.toByte()
        isAcceptSearch = parcel.readByte() != 0.toByte()
        campaignCode = parcel.readString()
        userId = parcel.readInt()
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProspectDTO> {
        override fun createFromParcel(parcel: Parcel): ProspectDTO {
            return ProspectDTO(parcel)
        }

        override fun newArray(size: Int): Array<ProspectDTO?> {
            return arrayOfNulls(size)
        }
    }
}