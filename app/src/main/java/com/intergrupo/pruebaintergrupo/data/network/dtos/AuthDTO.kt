package com.intergrupo.pruebaintergrupo.data.network.dtos

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class AuthDTO: Parcelable {

    @SerializedName("authToken")
    var token: String? = null
    @SerializedName("zone")
    var zone: String? = null
    @SerializedName("email")
    var email: String? = null


    constructor(parcel: Parcel){
        readFromParcel(parcel)
    }

    constructor(authToken: String, email: String, zone: String) {
        this.token = authToken
        this.zone = zone
        this.email = email
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(token)
        dest.writeString(zone)
        dest.writeString(email)
    }

    private fun readFromParcel(parcel: Parcel) {
        token = parcel.readString()
        zone = parcel.readString()
        email = parcel.readString()
    }

    companion object CREATOR : Parcelable.Creator<AuthDTO> {
        override fun createFromParcel(parcel: Parcel): AuthDTO {
            return AuthDTO(parcel)
        }

        override fun newArray(size: Int): Array<AuthDTO?> {
            return arrayOfNulls(size)
        }
    }

}