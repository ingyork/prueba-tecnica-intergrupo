package com.intergrupo.pruebaintergrupo.data.db.entities

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass


@RealmClass
open class ProspectEntity: RealmObject {

    @PrimaryKey
    var id: String? = null
    var name: String? = null
    var surname: String? = null
    var telephone: String? = null
    var schProspectIdentification: String? = null
    var address: String? = null
    var statusCd: Int? = null
    var zoneCode: String? = null
    var neighborhoodCode: String? = null
    var cityCode: String? = null
    var sectionCode: String? = null
    var roleId: Int? = null
    var appointableId: Int? = null
    var rejectedObservation: Int? = null
    var observation: String? = null
    var isDisable: Boolean? = null
    var isVisited: Boolean? = null
    var isCallcenter: Boolean? = null
    var isAcceptSearch: Boolean? = null
    var campaignCode: String? = null
    var userId: Int? = null





    constructor()

    constructor(id: String?, name: String?, surname: String?, telephone: String?, schProspectIdentification: String?,
                address: String?, statusCd: Int?, zoneCode: String?, neighborhoodCode: String?, cityCode: String?,
                sectionCode: String?, roleId: Int?, appointableId: Int?, rejectedObservation: Int?, observation: String?,
                isDisable: Boolean?, isVisited: Boolean?, isCallcenter: Boolean?, isAcceptSearch: Boolean?,
                campaignCode: String?, userId: Int?) {
        this@ProspectEntity.id = id
        this@ProspectEntity.name = name
        this@ProspectEntity.surname = surname
        this@ProspectEntity.telephone = telephone
        this@ProspectEntity.schProspectIdentification = schProspectIdentification
        this@ProspectEntity.address = address
        this@ProspectEntity.statusCd = statusCd
        this@ProspectEntity.zoneCode = zoneCode
        this@ProspectEntity.neighborhoodCode = neighborhoodCode
        this@ProspectEntity.cityCode = cityCode
        this@ProspectEntity.sectionCode = sectionCode
        this@ProspectEntity.roleId = roleId
        this@ProspectEntity.appointableId = appointableId
        this@ProspectEntity.rejectedObservation = rejectedObservation
        this@ProspectEntity.observation = observation
        this@ProspectEntity.isDisable = isDisable
        this@ProspectEntity.isVisited = isVisited
        this@ProspectEntity.isCallcenter = isCallcenter
        this@ProspectEntity.isAcceptSearch = isAcceptSearch
        this@ProspectEntity.campaignCode = campaignCode
        this@ProspectEntity.userId = userId
    }

}