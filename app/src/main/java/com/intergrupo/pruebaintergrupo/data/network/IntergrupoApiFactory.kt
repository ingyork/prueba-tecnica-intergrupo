package com.intergrupo.pruebaintergrupo.data.network

import com.intergrupo.pruebaintergrupo.utils.preference.PreferencesMethods
import com.intergrupo.pruebaintergrupo.utils.preference.Prefs
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL
import java.util.concurrent.TimeUnit


class IntergrupoApiFactory {

    init {
        initRetrofit()
    }

    companion object {

        var BASE_URL = "http://directotesting.igapps.co/"
        private var retrofit: Retrofit.Builder? = null
        private var retrofitWithToken: Retrofit.Builder? = null


        fun createAuth(): AuthService {
            return initRetrofit()?.build()?.create<AuthService>(AuthService::class.java)!!
        }

        fun createProspect(): ProspectService {
            return initRetrofitWithToken()
             ?.build()?.create<ProspectService>(ProspectService::class.java)!!
        }


        private fun initRetrofit() : Retrofit.Builder? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()

                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BASIC
                val client = OkHttpClient.Builder()
                        .addInterceptor(logging)
                        .build()

                retrofit?.client(client)
                        ?.baseUrl(BASE_URL)
                        ?.addConverterFactory(GsonConverterFactory.create())
                        ?.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            }

            return retrofit
        }


        private fun initRetrofitWithToken(): Retrofit.Builder? {
            if (retrofitWithToken == null) {
                retrofitWithToken = Retrofit.Builder()


                val access_token = Prefs.getString(PreferencesMethods.KeyPreferences.ACCESS_TOKEN, null)
                val okhttpclient = OkHttpClient().newBuilder()
                okhttpclient.addInterceptor { chain ->
                    val newRequest = chain.request().newBuilder()
                            .addHeader("token", access_token)
                            .build()
                    chain.proceed(newRequest)
                }.build()

                okhttpclient.readTimeout(30, TimeUnit.SECONDS)
                okhttpclient.connectTimeout(60, TimeUnit.SECONDS)

                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                okhttpclient.addInterceptor(interceptor)





                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BASIC
                val client = OkHttpClient.Builder()
                        .addInterceptor(logging)
                        .build()

                retrofitWithToken?.client(client)
                        ?.baseUrl(BASE_URL)
                        ?.client(okhttpclient.build())
                        ?.addConverterFactory(GsonConverterFactory.create())
                        ?.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            }
            return retrofitWithToken
        }

    }
}