package com.intergrupo.pruebaintergrupo.data.db.entities

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass


@RealmClass
open class AuthEntity: RealmObject {

    @PrimaryKey
    var id: Int = 0
    var authToken: String? = null
    var email: String? = null
    var isRemember: Boolean = false

    constructor()

    constructor(id: Int, token: String?, email: String?, isRemember: Boolean) {
        this.id = id
        authToken = token
        this@AuthEntity.email = email
        this@AuthEntity.isRemember = isRemember
    }
}