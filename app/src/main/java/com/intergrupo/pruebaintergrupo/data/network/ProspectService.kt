package com.intergrupo.pruebaintergrupo.data.network

import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import io.reactivex.Observable
import retrofit2.http.GET

interface ProspectService {

    @GET("sch/prospects.json")
    fun getProspects(): Observable<List<ProspectDTO>>
}