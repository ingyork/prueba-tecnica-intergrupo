package com.intergrupo.pruebaintergrupo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.intergrupo.pruebaintergrupo.R
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO

class ProspectAdapter(val data: List<ProspectDTO>) :
        RecyclerView.Adapter<ProspectAdapter.ViewHolder>() {

    interface OnInteractionItemProspect {
        fun OnEditProspect(prospectId: String)
    }

    lateinit var listener: OnInteractionItemProspect

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_prospect, parent, false)
        return ViewHolder(view, listener)
    }

    fun setOnInteractionItemProspect(listener: OnInteractionItemProspect) {
        this.listener = listener
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val prospect = data[position]
        holder.txtId.text = prospect.id
        holder.txtProspectIdentification.text = prospect.schProspectIdentification
        holder.txtFullName.text = "${prospect.name} ${prospect.surname}"
        holder.txtPhone.text = prospect.telephone

        val statusCode = prospect.statusCd
        when (statusCode) {
            0 -> {
                holder.ivStatus.setImageResource(R.drawable.ic_pending)
            }
            1 -> {
                holder.ivStatus.setImageResource(R.drawable.ic_approved)
            }
            2 -> {
                holder.ivStatus.setImageResource(R.drawable.ic_accepted)
            }
            3 -> {
                holder.ivStatus.setImageResource(R.drawable.ic_sad)
            }
            4 -> {
                holder.ivStatus.setImageResource(R.drawable.ic_disable)
            }
        }
    }

    class ViewHolder(itemView: View, listener: OnInteractionItemProspect?) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

        val txtProspectIdentification = itemView.findViewById(R.id.txtProspectIdentification) as TextView
        val txtFullName = itemView.findViewById<TextView>(R.id.txtFullName) as TextView
        val txtPhone = itemView.findViewById(R.id.txtPhone) as TextView
        val txtId = itemView.findViewById(R.id.txtId) as TextView
        val btnEdit = itemView.findViewById(R.id.btnEdit) as Button
        val ivStatus = itemView.findViewById(R.id.ivStatus) as ImageView
        var listener: OnInteractionItemProspect? = null

        init {
            this@ViewHolder.listener = listener
            btnEdit.setOnClickListener { onClick(itemView) }
        }

        override fun onClick(v: View?) {
            listener?.OnEditProspect(txtId.text.toString())
        }
    }

}