package com.intergrupo.pruebaintergrupo.di.main

import com.intergrupo.pruebaintergrupo.features.main.*
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    private lateinit var interactorListener: MainInteractor.OnMainListener

    constructor() {}

    constructor(listener: MainInteractor.OnMainListener){
        interactorListener = listener
    }

    @Provides fun provideMainPresenter(): IMainPresenter {
        return MainPresenter()
    }

    @Provides fun provideMainModel(): IMainInteractor {
        return MainInteractor(interactorListener)
    }
}