package com.intergrupo.pruebaintergrupo.di.login

import com.intergrupo.pruebaintergrupo.features.login.*
import dagger.Module
import dagger.Provides

@Module
class LoginModule {

    lateinit var interactorListener: LoginInteractor.OnLoginFinishedListener

    constructor()

    constructor(listener: LoginInteractor.OnLoginFinishedListener){
        interactorListener = listener
    }

    @Provides fun provideLoginPresenter(): ILoginPresenter {
        return LoginPresenter()
    }

    @Provides fun provideLoginModel(): ILoginInteractor {
        return LoginInteractor(interactorListener)
    }
}