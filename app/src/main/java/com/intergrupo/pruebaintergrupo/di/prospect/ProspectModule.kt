package com.intergrupo.pruebaintergrupo.di.prospect

import com.intergrupo.pruebaintergrupo.features.prospect.edit.EditProspectInteractor
import com.intergrupo.pruebaintergrupo.features.prospect.edit.EditProspectPresenter
import com.intergrupo.pruebaintergrupo.features.prospect.edit.IEditProspectInteractor
import com.intergrupo.pruebaintergrupo.features.prospect.edit.IEditProspectPresenter
import com.intergrupo.pruebaintergrupo.features.prospect.list.IListProspectInteractor
import com.intergrupo.pruebaintergrupo.features.prospect.list.IListProspectPresenter
import com.intergrupo.pruebaintergrupo.features.prospect.list.ListProspectInteractor
import com.intergrupo.pruebaintergrupo.features.prospect.list.ListProspectPresenter
import dagger.Module
import dagger.Provides

@Module
class ProspectModule {

    private lateinit var interactorListener: ListProspectInteractor.OnProspectListener
    private lateinit var editInteractorListener: EditProspectInteractor.OnEditProspectListener

    constructor() {}

    constructor(listener: ListProspectInteractor.OnProspectListener){
        interactorListener = listener
    }

    constructor(listener: EditProspectInteractor.OnEditProspectListener){
        editInteractorListener = listener
    }

    @Provides fun provideListProspectPresenter(): IListProspectPresenter{
        return ListProspectPresenter()
    }

    @Provides fun provideListProspectModel(): IListProspectInteractor {
        return ListProspectInteractor(interactorListener)
    }

    @Provides fun provideEditProspectPresenter(): IEditProspectPresenter {
        return EditProspectPresenter()
    }

    @Provides fun provideEditProspectModel(): IEditProspectInteractor {
        return EditProspectInteractor(editInteractorListener)
    }
}