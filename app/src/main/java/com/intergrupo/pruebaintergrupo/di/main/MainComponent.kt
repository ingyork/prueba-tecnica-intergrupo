package com.intergrupo.pruebaintergrupo.di.main

import com.intergrupo.pruebaintergrupo.features.main.MainActivity
import com.intergrupo.pruebaintergrupo.features.main.MainInteractor
import com.intergrupo.pruebaintergrupo.features.main.MainPresenter
import com.intergrupo.pruebaintergrupo.features.prospect.edit.EditProspectActivity
import dagger.Component

@Component(modules = arrayOf(MainModule::class))
interface MainComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(mainPresenter: MainPresenter)
}