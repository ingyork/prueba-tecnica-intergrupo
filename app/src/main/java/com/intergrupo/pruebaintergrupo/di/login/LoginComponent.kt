package com.intergrupo.pruebaintergrupo.di.login

import com.intergrupo.pruebaintergrupo.features.login.LoginActivity
import com.intergrupo.pruebaintergrupo.features.login.LoginInteractor
import com.intergrupo.pruebaintergrupo.features.login.LoginPresenter
import dagger.Component

@Component(modules = arrayOf(LoginModule::class))
interface LoginComponent {
    fun inject(loginActivity: LoginActivity)
    fun inject(loginPresenter: LoginPresenter)
    fun inject(loginModel: LoginInteractor)
    fun inject(loginListenerInteractor: LoginInteractor.OnLoginFinishedListener)
}