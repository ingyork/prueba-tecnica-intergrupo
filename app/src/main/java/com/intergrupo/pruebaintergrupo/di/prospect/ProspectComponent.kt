package com.intergrupo.pruebaintergrupo.di.prospect

import com.intergrupo.pruebaintergrupo.features.prospect.edit.EditProspectActivity
import com.intergrupo.pruebaintergrupo.features.prospect.edit.EditProspectPresenter
import com.intergrupo.pruebaintergrupo.features.prospect.list.ListProspectFragment
import com.intergrupo.pruebaintergrupo.features.prospect.list.ListProspectPresenter
import dagger.Component

@Component(modules = arrayOf(ProspectModule::class))
interface ProspectComponent {
    fun inject(listProspectFragment: ListProspectFragment)
    fun inject(editProspectActivity: EditProspectActivity)
    fun inject(listProspectPresenter: ListProspectPresenter)
    fun inject(editProspectPresenter: EditProspectPresenter)
}