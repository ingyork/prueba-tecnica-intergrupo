package com.intergrupo.pruebaintergrupo

import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import com.intergrupo.pruebaintergrupo.features.prospect.list.IListProspectPresenter
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class ListProspectPresenterTest {

    @Mock
    var prospectPresenter: IListProspectPresenter? = null


    @Test
    fun listProspectNotEmpty() {

        val list = listOf<ProspectDTO>()

        doNothing().`when`(prospectPresenter)?.listProspects()
        doNothing().`when`(prospectPresenter)?.onSuccessListProspects(anyList())

        prospectPresenter?.listProspects()
        prospectPresenter?.onSuccessListProspects(list)

        verify(prospectPresenter, times(1))?.onSuccessListProspects(list)
    }

    @Test
    fun listProspectEmpty() {
        doNothing().`when`(prospectPresenter)?.listProspects()
        doNothing().`when`(prospectPresenter)?.onEmptyData()

        prospectPresenter?.listProspects()
        prospectPresenter?.onEmptyData()

        verify(prospectPresenter, times(1))?.onEmptyData()
    }

}