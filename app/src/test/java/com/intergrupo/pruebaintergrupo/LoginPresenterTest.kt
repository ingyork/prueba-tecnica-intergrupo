package com.intergrupo.pruebaintergrupo

import com.intergrupo.pruebaintergrupo.features.login.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class LoginPresenterTest {

    @Mock
    var loginPresenter: ILoginPresenter? = null


    @Test
    fun isRememberSuccess() {
        doNothing().`when`(loginPresenter)?.isRememberSession()
        doNothing().`when`(loginPresenter)?.onRememberSession()

        loginPresenter?.isRememberSession()
        loginPresenter?.onRememberSession()

        verify(loginPresenter, times(1))?.onRememberSession()
    }

    @Test(expected = Exception::class)
    fun isRememberFalse() {
        doThrow().`when`(loginPresenter)?.isRememberSession()
        loginPresenter?.isRememberSession()
        verifyNoMoreInteractions(loginPresenter)
    }

    @Test
    fun emailValid() {
        `when`(loginPresenter?.validateEmail(anyString())).thenReturn(true)
        val response = loginPresenter?.validateEmail("ingmaster.jc@gmail.com")
        assert(response!!)
    }

    @Test
    fun emailNotValid() {
        `when`(loginPresenter?.validateEmail(anyString())).thenReturn(false)
        val response = loginPresenter?.validateEmail("abc")
        assert(!response!!)
    }

    @Test
    fun credentialsValid() {
        `when`(loginPresenter?.validateCredentials(anyString(), anyString())).thenReturn(true)
        val response = loginPresenter?.validateCredentials("", "")
        assert(response!!)
    }

    @Test
    fun credentialsNotValid() {
        `when`(loginPresenter?.validateCredentials(anyString(), anyString())).thenReturn(false)
        val response = loginPresenter?.validateCredentials("", "")
        assert(!response!!)
    }

    @Test
    fun loginSuccess() {
        doNothing().`when`(loginPresenter)?.login(anyString(), anyString(), anyBoolean())
        loginPresenter?.login("", "", true)
        loginPresenter?.onSuccess()
        verify(loginPresenter, times(1))?.onSuccess()
    }

    @Test
    fun loginFail() {
        doNothing().`when`(loginPresenter)?.login(anyString(), anyString(), anyBoolean())
        loginPresenter?.login("", "", true)
        loginPresenter?.loginFail()
        verify(loginPresenter, times(1))?.loginFail()
    }
}