package com.intergrupo.pruebaintergrupo

import com.intergrupo.pruebaintergrupo.features.main.IMainPresenter
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {

    @Mock
    var mainPresenter: IMainPresenter? = null


    @Test
    fun logoutSuccess() {
        Mockito.doNothing().`when`(mainPresenter)?.logout()
        Mockito.doNothing().`when`(mainPresenter)?.onLogout()

        mainPresenter?.logout()
        mainPresenter?.onLogout()

        Mockito.verify(mainPresenter, Mockito.times(1))?.onLogout()
    }

    @Test(expected = Exception::class)
    fun logoutFail() {
        Mockito.doThrow().`when`(mainPresenter)?.logout()
        mainPresenter?.logout()

        Mockito.verify(mainPresenter)
    }

}