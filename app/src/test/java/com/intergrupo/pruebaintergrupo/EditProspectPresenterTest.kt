package com.intergrupo.pruebaintergrupo

import com.intergrupo.pruebaintergrupo.data.db.entities.ProspectEntity
import com.intergrupo.pruebaintergrupo.data.network.dtos.ProspectDTO
import com.intergrupo.pruebaintergrupo.features.prospect.edit.IEditProspectPresenter
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class EditProspectPresenterTest {

    @Mock
    var prospectPresenter: IEditProspectPresenter? = null


    @Test
    fun getProspectSuccess() {

        val p = ProspectDTO()
        doNothing().`when`(prospectPresenter)?.getProspectById(anyString())
        doNothing().`when`(prospectPresenter)?.OnGetProspectById(any())

        prospectPresenter?.getProspectById("")
        prospectPresenter?.OnGetProspectById(p)

        verify(prospectPresenter, times(1))?.OnGetProspectById(p)
    }

    @Test(expected = Exception::class)
    fun getProspectFail() {
        val p = ProspectDTO()
        doThrow().`when`(prospectPresenter)?.getProspectById(anyString())
        prospectPresenter?.getProspectById("")
        verify(prospectPresenter)
    }

    @Test
    fun updateProspectSuccess(){

        val p = ProspectEntity()
        p.userId = 0
        p.name = ""

        doNothing().`when`(prospectPresenter)?.updateProspect(p)
        doNothing().`when`(prospectPresenter)?.OnUpdateProspect()

        prospectPresenter?.updateProspect(p)
        prospectPresenter?.OnUpdateProspect()

        verify(prospectPresenter, times(1))?.OnUpdateProspect()
    }
}